var menu = (function($) {

  var $menuPos = $(".link-scroll"),
      $marginVal;

  scrollPage = function (e) {
    e.preventDefault();
    var $elem = $($(this).attr('href'));

    if ($(window).width() < 768) {
      $marginVal = 40;
    } else {
      $marginVal = 80;
    }

    $('html, body').stop().animate({
        scrollTop: $elem.position().top - $marginVal
    }, 2000);
  };

  init = function () {
    $menuPos.on('click', scrollPage);
  };

  return {
    init: init,
  };

})(window.jQuery);
