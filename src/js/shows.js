var shows = (function ($) {

  var $openBtn = $('.show-form'),
      $closeBtn = $('.hide-form');

  showForm = function () {
    $(this).closest('.text').addClass('form');
  };

  hideForm = function () {
    $(this).closest('.text').removeClass('form');
  };

  init = function () {
    $openBtn.on('click', showForm);
    $closeBtn.on('click', hideForm);
  };

  return {
    init: init,
  };

})(window.jQuery);
