var header = (function($) {

  var $burger = $('.burger-icon'),
      $items = $burger.closest('.menu').find('ul');

  openClose = function (ev) {
    ev.stopPropagation();
    $burger.toggleClass('opened');
    $items.toggleClass('opened');
    $('.overlay').toggleClass('showed');
  };

  var close = function (ev) {
    ev.stopPropagation();
    $burger.removeClass('opened');
    $items.removeClass('opened');
    $('.overlay').removeClass('showed');
  };

  init = function () {
     $burger.on('click', openClose);
     $(window).on('click resize', close);
  };

  return {
    init: init,
  };

})(window.jQuery);
