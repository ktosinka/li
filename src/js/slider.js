var slider = (function($) {
  var $gallery = $("#gallery"),
      $next,
      direction = true,
      $galleryImg = $('#gallery .img'),
      gallerySlide;

  slideSwitch = function () {
    var $active = $('#gallery .img.active');
    if (direction) {
      $next =  $active.next().hasClass('img') ? $active.next() : $galleryImg.first();
    } else {
      // clearInterval(gallerySlider);
      $next =  $active.prev().hasClass('img') ? $active.prev() : $galleryImg.last();
      direction = true;
      //setTimeout( setInterval(slideSwitch, 5000), 1000);
    }

    $next.addClass('active');
    $active.removeClass('active');
  };

  init = function () {
    gallerySlider = setInterval(slideSwitch, 3000);

    $('#gallery .prev').on('click', function () {
      direction = false;
      slideSwitch();
    });
    $('#gallery .next').on('click', slideSwitch);
  };

  return {
    init: init,
  };

})(window.jQuery);
