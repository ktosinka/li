var email = (function($) {

  showPopup = function (message) {
    $divMessage = '<div class="btn">' + message + '</div>';
    $('.overlay').addClass('showed').append($divMessage);

    setTimeout(function() {
        $('.overlay').removeClass('showed');
    }, 3600);
  };

  init = function () {
    emailjs.init('user_pWzHVB4A9I0sOk4dBJ3yu');
    console.log('Initialize finito!');

   $('#btn_swing_form_submit').on('click', function(){
     var message = prepareMessage('#form_swing', 'Electro Swing');
     sendQuestion(message);
     clearFields('#form_swing');
   });

   $('#btn_violin_form_submit').on('click', function(){
     var message = prepareMessage('#form_violin', 'Magic Violin');
     sendQuestion(message);
     clearFields('#form_violin');
   });

   $('#btn_led_form_submit').on('click', function(){
     var message = prepareMessage('#form_led', 'Pokaz LED');
     sendQuestion(message);
     clearFields('#form_led');
   });

   $('#btn_buugeng_form_submit').on('click', function(){
     var message = prepareMessage('#form_buugeng', 'Buugeng');
     sendQuestion(message);
     clearFields('#form_buugeng');
   });

   $('#btn_swing_contact_submit').on('click', function(){
     var message = prepareMessage('#contact', 'KONTAKT');
     sendQuestion(message);
     clearFields('#contact');
   });
  };



  var messageType = {
    'question': 1,
    'question_Magic_Violin': 2,
    'question_Swing_Life': 3
  };


  var clearFields = function (formID) {
    $(formID).children('input')[0].value= '';
    $(formID).children('input')[1].value= '';
    $(formID).children('textarea')[0].value= '';
  };

  var prepareMessage = function (formID, questionType) {

   var name = $(formID).children('input')[0].value;
   var email = $(formID).children('input')[1].value;
   var description = $(formID).children('textarea')[0].value;

    var message = new Object();
    message.showType = questionType;
    message.sendDate = getTodayDate();
    message.name = name;
    message.email = email;
    message.showDate = 'brak daty';
    message.description = description;
    console.log(message);
    return message;
  };

  var getTodayDate = function () {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
      dd = '0' + dd
    }

    if (mm < 10) {
      mm = '0' + mm
    }

    today = mm.toString() + '.' + dd.toString() + '.' + yyyy.toString();
    return today;
  };

  var sendQuestion = function (questionObject) {
    //send to us
    sendEmail(questionObject, 'question_show');
    //send to client
    sendEmail(questionObject, 'question_answer');
  };

  var sendEmail = function (messageObj, messageType) {
    emailjs.send('lividusignis_gmail', messageType, messageObj)
      .then(function(response) {
        console.warn('SUCCESS. status=%d, text=%s', response.status, response.text);
        if (messageType == 'question_show') {
          showPopup('Wiadomość wysłana.')
        }

      }, function(err) {
        console.error('FAILED. error=', err);
          if (messageType == 'question_show') {
            showPopup('Niewysłano Wiadomości.');
          }
      });
  };

  return {
    init: init
  };

})(window.jQuery);
