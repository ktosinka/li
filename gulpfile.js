var gulp = require('gulp');
var connect = require('gulp-connect');
var watch = require('gulp-watch');
var useref = require('gulp-useref');
var jshint = require('gulp-jshint');
var stylish = require('jshint-stylish');
var uglify = require('gulp-uglify');

var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var cssmin = require('gulp-cssmin');

var gulpif = require('gulp-if');

var config = {
    paths: {
        html: {
            src:  ['src/**/*.html']
        },
        javascript: {
            src:  ['src/js/**/*.js'],
            dest: 'src/js'
        },
        scss: {
            src: ['src/scss/**/*.scss'],
            dest: 'src/css'
        },
        images: {
            src: ['src/img/**/*'],
            dest: 'dist/img'
        },
        fonts: {
            src: ['src/fonts/**/*'],
            dest: 'dist/fonts'
        }
    }
};

gulp.task('connect', function() {
  connect.server({
    root: 'src',// wskazanie lokalizacji, z ktorej odpalana jest strona na http://localhost:8080/
    livereload: true
  });
});

gulp.task('style', function(){
  return gulp.src(config.paths.scss.src)
    .pipe(sass()
    .on('error', sass.logError))

    .pipe(autoprefixer())

    .pipe(connect.reload())

    .pipe(gulp.dest(config.paths.scss.dest));
});

gulp.task('lint', function() {
  return gulp.src(config.paths.javascript.src)
    .pipe(jshint())
    .pipe(jshint.reporter(stylish))
    .pipe(connect.reload());
});

gulp.task('watch', function () {
  gulp.watch(config.paths.html.src, ['html']);
  gulp.watch(config.paths.scss.src, ['style']);
  gulp.watch(config.paths.javascript.src, ['lint']);
});

gulp.task('html', function(){
   return gulp.src(config.paths.html.src)
     .pipe(connect.reload());
});

gulp.task('index', function () {

  return gulp.src(config.paths.html.src)
    .pipe(useref())
    .pipe(gulpif('*.js', uglify()))
    .pipe(gulpif('*.css', cssmin()))
    .pipe(gulp.dest('dist'));
});

gulp.task('img', function () {

  return gulp.src(config.paths.images.src)
    .pipe(gulp.dest(config.paths.images.dest));
});

gulp.task('copy', function () {

  gulp.src('src/*.{xml,txt,htaccess}')
    .pipe(gulp.dest('dist'))
  gulp.src('src/js/lib/*.js')
    .pipe(gulp.dest('dist/js/lib'));
});

gulp.task('fonts', function () {

  return gulp.src(config.paths.fonts.src)
    .pipe(gulp.dest(config.paths.fonts.dest));
});

gulp.task('default', ['connect', 'style', 'lint', 'watch']);
gulp.task('project', ['index', 'img', 'fonts', 'copy']);
